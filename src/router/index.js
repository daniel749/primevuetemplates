import { createRouter, createWebHashHistory } from "vue-router";
import Home from "../views/Home.vue";
import templatePage from "../views/templatePage.vue";
import stepOne from "../components/stepperPages/stepOne.vue";
import stepTwo from "../components/stepperPages/stepTwo.vue";
import DesignSystem from "../views/DesignSystem.vue";
import Dashboard from "../views/Dashboard.vue";
import TableView from "../views/TableView.vue";

const dashboard = { template: '<div>dashbo</div>' }
const sales = { template: '<div>sales</div>' }
const categories = {
  template: '<div>User {{ $route.params.id }}</div>'
}
const routes = [
  {
    path: "/home",
    name: "templatePage",
    component: templatePage,
    //children routes
    children: [
      // {
      //   path: "",
      //   name: "tabOne",
      //   component: tabOne,
      // },
      {
        path: "/",
        name: "StepOne",
        component: stepOne,
        // component:
      },
      {
        path: "/stepTwo",
        name: "StepTwo",
        component: stepTwo,
      },
    ],
  },
  {
    path: "/Home",
    name: "Home",
    component: Home,
  },
  {
    path: "/DesignSystem",
    name: "DesignSystem",
    component: DesignSystem,
     //children routes
     children: [
      // {
      //   path: "",
      //   name: "tabOne",
      //   component: tabOne,
      // },
      {
        path: "/",
        name: "StepOne",
        component: stepOne,
        // component:
      },
      {
        path: "/stepTwo",
        name: "StepTwo",
        component: stepTwo,
      },
    ],
  },
  {
      path:"/Dashboard",
      name:"Dashboard",
      component: Dashboard,
  },
  { path: '/Dasboard/dashboard', component: dashboard },
  { path: '/Dasboard/sales', component: sales },
  // dynamic segments start with a colon
  { path: '/Dashboard/:id', component: categories },
  {
    path: "/TableView",
    name: "TableView",
    component: TableView,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
