import { createApp } from "vue";
import ToastService from "primevue/toastservice";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import randomColor from "randomcolor";
// import VueRandomColor from "vue-randomcolor";
import PrimeVue from "primevue/config";
import "primevue/resources/themes/saga-blue/theme.css";
import "primevue/resources/primevue.min.css";
import "primeicons/primeicons.css";
import "primeflex/primeflex.css";

import HelloWorld from "./components/HelloWorld";
import PrimeVueBreadCrumb from "./components/PrimeVueCommons/PrimeVueBreadCrumb.vue";
import PrimeVueDropDown from "./components/PrimeVueCommons/PrimeVueDropDown.vue";
import PrimeVueRadioButtons from "./components/PrimeVueCommons/PrimeVueRadioButtons.vue";
import PrimeVueToggle from "./components/PrimeVueCommons/PrimeVueToggle.vue";
import ratings from "./components/PrimeVueCommons/ratings.vue";
import buttons from "./components/PrimeVueCommons/buttons.vue";
import dialogs from "./components/PrimeVueCommons/dialogs.vue";
import slider from "./components/PrimeVueCommons/slider.vue";
import InputTextField from "./components/PrimeVueCommons/TextField.vue";
import TextArea from "./components/PrimeVueCommons/TextArea.vue";
import CalendarField from "./components/PrimeVueCommons/CalendarField.vue";
import CheckBox from "./components/PrimeVueCommons/CheckBox.vue";
import Fileupload from "./components/PrimeVueCommons/FileUpload.vue";
import steps from "./components/PrimeVueCommons/steps.vue";
import stepOne from "./components/stepperPages/stepOne.vue";
import stepTwo from "./components/stepperPages/stepTwo.vue";
import toastmessages from "./components/PrimeVueCommons/toastmessages.vue";
import tabMenu from "./components/PrimeVueCommons/tabMenu.vue";
import tabOne from "./components/tabviewPages/tabOne.vue";
import tabTwo from "./components/tabviewPages/tabTwo.vue";
import accordion from "./components/PrimeVueCommons/accordion.vue";
import paginations from "./components/PrimeVueCommons/paginations.vue";
import DesignSystem from "./views/DesignSystem.vue";
import ToolTip from "./components/PrimeVueCommons/ToolTip.vue"
import SearchInputField from "./components/PrimeVueCommons/SearchInputField.vue"
import Toaster from "./components/PrimeVueCommons/Toaster.vue"
import Carousels from "./components/PrimeVueCommons/Carousels.vue"
import ChartTemplate from "./components/PrimeVueCommons/ChartTemplate.vue"
import Table from "./components/PrimeVueCommons/Table.vue"
import PassWord from "./components/PrimeVueCommons/PassWord.vue"
import TabView from 'primevue/tabview';
import TabPanel from 'primevue/tabpanel';
import MultiSelectChip from "./components/PrimeVueCommons/MultiSelectChip.vue"
//for table
import DataTables from "./components/PrimeVueCommons/DataTables.vue";
import Badges from "./components/PrimeVueCommons/Badges.vue";
import CustomSpeedDial from "./components/PrimeVueCommons/CustomSpeedDial.vue";
import AvatarLabel from "./components/PrimeVueCommons/AvatarLabel.vue";
// import ToolTip from "./components/PrimeVueCommons/ToolTip.vue";
// import SearchInputField from "./components/PrimeVueCommons/SearchInputField.vue";
// import Toaster from "./components/PrimeVueCommons/Toaster.vue";
// import Carousels from "./components/PrimeVueCommons/Carousels.vue";
// import ChartTemplate from "./components/PrimeVueCommons/ChartTemplate.vue";
// import Table from "./components/PrimeVueCommons/Table.vue";
// import PassWord from "./components/PrimeVueCommons/PassWord.vue";

const app = createApp(App);

app.component("HelloWorld", HelloWorld);
app.component("PrimeVueBreadCrumb", PrimeVueBreadCrumb);
app.component("PrimeVueDropDown", PrimeVueDropDown);
app.component("PrimeVueRadioButtons", PrimeVueRadioButtons);
app.component("PrimeVueToggle", PrimeVueToggle);
app.component("ratings", ratings);
app.component("buttons", buttons);
app.component("dialogs", dialogs);
app.component("slider", slider);
app.component("InputTextField", InputTextField);
app.component("TextArea", TextArea);
app.component("CalendarField", CalendarField);
app.component("CheckBox", CheckBox);
app.component("Fileupload", Fileupload);
app.component("steps", steps);
app.component("stepOne", stepOne);
app.component("stepTwo", stepTwo);
app.component("stepOne", stepOne);
app.component("toastmessages", toastmessages);
app.component("tabMenu", tabMenu);
app.component("tabOne", tabOne);
app.component("tabTwo", tabTwo);
app.component("accordion", accordion);
app.component("paginations", paginations);
app.component("ToolTip", ToolTip);
app.component("SearchInputField", SearchInputField);
app.component("Toaster", Toaster);
app.component("Carousels", Carousels);
app.component("ChartTemplate", ChartTemplate);
app.component("Table", Table);
app.component("DesignSystem", DesignSystem);
app.component("PassWord" , PassWord)
//app.component('TabMenu', TabMenu);
app.component('TabView', TabView);
app.component('TabPanel', TabPanel);
app.component('MultiSelectChip' , MultiSelectChip)

app.component("DataTables", DataTables);
app.component("Badges", Badges);
app.component("CustomSpeedDial", CustomSpeedDial);
app.component("AvatarLabel", AvatarLabel);
app.component("PassWord", PassWord);
app
  .use(router)
  .use(store)
  .use(randomColor)
  .use(PrimeVue)
  .use(ToastService)
  .mount("#app");
