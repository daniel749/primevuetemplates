import { createStore } from "vuex";

export default createStore({
  //store the date eg c0mmon data to be shared amongst componentss
  state: {
    //data for table
    tableData: [
      {
        Actions: "",
        "User Id": "123",
        Username: "Jasmine",
        Role: "Accounts Clerk",
        "Contact No": "123456",
        Email: "jasmine@gmail.com",
        "Access Level": "1",
        Act: "",
        Status: true,
      },
      {
        Actions: "",
        "User Id": "124",
        Username: "Catherine",
        Role: "Delivery Girl",
        "Contact No": "123467",
        Email: "catherine@gmail.com",
        "Access Level": "2",
        Act: "",
        Status: true,
      },
      {
        Actions: "",
        "User Id": "125",
        Username: "Tim Cook",
        Role: "Quality Checker",
        "Contact No": "123468",
        Email: "timCook@gmail.com",
        "Access Level": "4",
        Act: "",
        Status: false,
      },
    ],
    //data for table columns
    tableColumns: [
      { field: "Actions", header: "Actions" },
      { field: "User Id", header: "User Id" },
      { field: "Username", header: "Username" },
      { field: "Role", header: "Role" },
      { field: "Contact No", header: "Contact No." },
      { field: "Email", header: "Email" },
      { field: "Access Level", header: "Access Level" },
      { field: "Act", header: "Act" },
      { field: "Status", header: "Status" },
    ],
    // data for badges
    badgeColors: ["level_1", "level_2", "level_3", "level_4"],
    badgeData: [{ color: "" }, { color: "" }, { color: "" }, { color: "" }],
    //data for dropdowns
    dropDownData: [
      { name: "New York", code: "NY" },
      { name: "Rome", code: "RM" },
      { name: "London", code: "LDN" },
      { name: "Istanbul", code: "IST" },
      { name: "Paris", code: "PRS" },
    ],
    //data for bread crumbs
    home: { icon: "pi pi-home" },
    items: [
      { label: "Computer" },
      { label: "Notebook" },
      { label: "Accessories" },
      { label: "Backpacks" },
      { label: "Item" },
    ],
    //data for radio button
    categories: [
      { name: "Accounting", key: "A" },
      { name: "Marketing", key: "M" },
      { name: "Production", key: "P" },
      { name: "Research", key: "R" },
    ],
    normalRadio: [{ name: "normal", key: "n" }],
    disabledRadio: [{ name: "disabled", key: "n" }],
    selectedRadio: [{ name: "selected", key: "s" }],
    //data for toggle
    toggleData: [{ type: "Email" }, { type: "SMS" }],
    //data for checkboxes
    checkboxData: [
      { name: "Accounting", key: "A" },
      { name: "Marketing", key: "M" },
      { name: "Production", key: "P" },
      { name: "Research", key: "R" },
    ],
    normalCheckBox: [{ name: "normal", key: "n" }],
    disabledCheckBox: [{ name: "disabled", key: "n" }],
    selectedCheckBox: [{ name: "selected", key: "s" }],
    //data for stepper
    stepperData: [
      {
        label: "Step One",
        to: "/",
      },
      {
        label: "Step Two",
        to: "/stepTwo",
      },
    ],
    //data for dialog
    displayDialog: false,
    //data for tab-menu
    tabMenuData: [
      { label: "Tab One", icon: "pi pi-fw pi-home", tab: "tabOne" },
      { label: "Tab Two", icon: "pi pi-fw pi-calendar", tab: "tabTwo" },
    ],
    //data for accordian
    accordionData: [
      {
        title: "Opener One",
        content: "Surprise!",
      },
      {
        title: "Opener Two",
        content: "Happy!",
      },
    ],
    //data for Carousels
    carouselData: [
      {
        id: "1000",
        code: "f230fh0g3",
        name: "Bamboo Watch",
        description: "Product Description",
        image: "bamboo-watch.jpg",
        price: 65,
        category: "Accessories",
        quantity: 24,
        inventoryStatus: "INSTOCK",
        rating: 5,
        imgSrc:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRvW3pHN63jgZABpn2ijaij-ofULCeAs-Hivg&usqp=CAU",
      },
      {
        id: "1001",
        code: "nvklal433",
        name: "Black Watch",
        description: "Product Description",
        image: "black-watch.jpg",
        price: 72,
        category: "Accessories",
        quantity: 61,
        inventoryStatus: "INSTOCK",
        rating: 4,
        imgSrc:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRE-Kf5Lwo6fK8fYaJiPmeF-D1NrAgo6bFzFg&usqp=CAU",
      },
      {
        id: "1002",
        code: "zz21cz3c1",
        name: "Blue Band",
        description: "Product Description",
        image: "blue-band.jpg",
        price: 79,
        category: "Fitness",
        quantity: 2,
        inventoryStatus: "LOWSTOCK",
        rating: 3,
        imgSrc:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSdue7RXRVL-2sFvvfBS_hx5SGGRMUyFe70Cw&usqp=CAU",
      },
      {
        id: "1003",
        code: "244wgerg2",
        name: "Blue T-Shirt",
        description: "Product Description",
        image: "blue-t-shirt.jpg",
        price: 29,
        category: "Clothing",
        quantity: 25,
        inventoryStatus: "INSTOCK",
        rating: 5,
        imgSrc:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRvW3pHN63jgZABpn2ijaij-ofULCeAs-Hivg&usqp=CAU",
      },

      {
        id: "1005",
        code: "av2231fwg",
        name: "Brown Purse",
        description: "Product Description",
        image: "brown-purse.jpg",
        price: 120,
        category: "Accessories",
        quantity: 0,
        inventoryStatus: "OUTOFSTOCK",
        rating: 4,
        imgSrc:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRE-Kf5Lwo6fK8fYaJiPmeF-D1NrAgo6bFzFg&usqp=CAU",
      },
      {
        id: "1006",
        code: "bib36pfvm",
        name: "Chakra Bracelet",
        description: "Product Description",
        image: "chakra-bracelet.jpg",
        price: 32,
        category: "Accessories",
        quantity: 5,
        inventoryStatus: "LOWSTOCK",
        rating: 3,
        imgSrc:
          "https://oyster.ignimgs.com/wordpress/stg.ign.com/2020/05/sale_22062_primary_image_wide.jpg",
      },
      {
        id: "1007",
        code: "mbvjkgip5",
        name: "Galaxy Earrings",
        description: "Product Description",
        image: "galaxy-earrings.jpg",
        price: 34,
        category: "Accessories",
        quantity: 23,
        inventoryStatus: "INSTOCK",
        rating: 5,
        imgSrc:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQrl1fm5Iy1bxarUQbKPPQBxuezzfVSjHMznQ&usqp=CAU",
      },
      {
        id: "1004",
        code: "h456wer53",
        name: "Bracelet",
        description: "Product Description",
        image: "bracelet.jpg",
        price: 15,
        category: "Accessories",
        quantity: 73,
        inventoryStatus: "INSTOCK",
        rating: 4,
        imgSrc:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRvW3pHN63jgZABpn2ijaij-ofULCeAs-Hivg&usqp=CAU",
      },
      {
        id: "1008",
        code: "vbb124btr",
        name: "Game Controller",
        description: "Product Description",
        image: "game-controller.jpg",
        price: 99,
        category: "Electronics",
        quantity: 2,
        inventoryStatus: "LOWSTOCK",
        rating: 4,
        imgSrc:
          "https://www.codemotion.com/magazine/wp-content/uploads/2020/12/christopher-gower-m_HRfLhgABo-unsplash-896x504.jpg",
      },
      {
        id: "1009",
        code: "cm230f032",
        name: "Gaming Set",
        description: "Product Description",
        image: "gaming-set.jpg",
        price: 299,
        category: "Electronics",
        quantity: 63,
        inventoryStatus: "INSTOCK",
        rating: 3,
        imgSrc:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQlCskl3viQobQF8cg8AvfaM6dzWohPQeNo4Q&usqp=CAU",
      },
    ],
    responsiveOptions: [
      {
        breakpoint: "1024px",
        numVisible: 3,
        numScroll: 1,
      },
      {
        breakpoint: "600px",
        numVisible: 2,
        numScroll: 1,
      },
      {
        breakpoint: "480px",
        numVisible: 1,
        numScroll: 1,
      },
    ],
    //Line-chart data
    LineChartData: {
      basicData: {
        labels: [
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
        ],
        datasets: [
          {
            label: "First Dataset",
            data: [65, 59, 80, 81, 56, 55, 40],
            fill: false,
            borderColor: "#42A5F5",
            tension: 0.4,
          },
          {
            label: "Second Dataset",
            data: [28, 48, 40, 19, 86, 27, 90],
            fill: false,
            borderColor: "#FFA726",
            tension: 0.4,
          },
        ],
      },
      multiAxisData: {
        labels: [
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
        ],
        datasets: [
          {
            label: "Dataset 1",
            fill: false,
            borderColor: "#42A5F5",
            yAxisID: "y",
            tension: 0.4,
            data: [65, 59, 80, 81, 56, 55, 10],
          },
          {
            label: "Dataset 2",
            fill: false,
            borderColor: "#00bb7e",
            yAxisID: "y1",
            tension: 0.4,
            data: [28, 48, 40, 19, 86, 27, 90],
          },
        ],
      },
      lineStylesData: {
        labels: [
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
        ],
        datasets: [
          {
            label: "First Dataset",
            data: [65, 59, 80, 81, 56, 55, 40],
            fill: false,
            tension: 0.4,
            borderColor: "#42A5F5",
          },
          {
            label: "Second Dataset",
            data: [28, 48, 40, 19, 86, 27, 90],
            fill: false,
            borderDash: [5, 5],
            tension: 0.4,
            borderColor: "#66BB6A",
          },
          {
            label: "Third Dataset",
            data: [12, 51, 62, 33, 21, 62, 45],
            fill: true,
            borderColor: "#FFA726",
            tension: 0.4,
            backgroundColor: "rgba(255,167,38,0.2)",
          },
        ],
      },
      basicOptions: {
        plugins: {
          legend: {
            labels: {
              color: "#495057",
            },
          },
        },
        scales: {
          x: {
            ticks: {
              color: "#495057",
            },
            grid: {
              color: "#ebedef",
            },
          },
          y: {
            ticks: {
              color: "#495057",
            },
            grid: {
              color: "#ebedef",
            },
          },
        },
      },
      multiAxisOptions: {
        stacked: false,
        plugins: {
          legend: {
            labels: {
              color: "#495057",
            },
          },
        },
        scales: {
          x: {
            ticks: {
              color: "#495057",
            },
            grid: {
              color: "#ebedef",
            },
          },
          y: {
            type: "linear",
            display: true,
            position: "left",
            ticks: {
              color: "#495057",
            },
            grid: {
              color: "#ebedef",
            },
          },
          y1: {
            type: "linear",
            display: true,
            position: "right",
            ticks: {
              color: "#495057",
            },
            grid: {
              drawOnChartArea: false,
              color: "#ebedef",
            },
          },
        },
      },
    },
    //bar-chart data
    BarChartData: {
      basicData: {
        labels: [
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
        ],
        datasets: [
          {
            label: "My First dataset",
            backgroundColor: "#42A5F5",
            data: [65, 59, 80, 81, 56, 55, 40],
          },
          {
            label: "My Second dataset",
            backgroundColor: "#FFA726",
            data: [28, 48, 40, 19, 86, 27, 90],
          },
        ],
      },
      multiAxisData: {
        labels: [
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
        ],
        datasets: [
          {
            label: "Dataset 1",
            backgroundColor: [
              "#EC407A",
              "#AB47BC",
              "#42A5F5",
              "#7E57C2",
              "#66BB6A",
              "#FFCA28",
              "#26A69A",
            ],
            yAxisID: "y-axis-1",
            data: [65, 59, 80, 81, 56, 55, 10],
          },
          {
            label: "Dataset 2",
            backgroundColor: "#78909C",
            yAxisID: "y-axis-2",
            data: [28, 48, 40, 19, 86, 27, 90],
          },
        ],
      },
      stackedData: {
        labels: [
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
        ],
        datasets: [
          {
            type: "bar",
            label: "Dataset 1",
            backgroundColor: "#42A5F5",
            data: [50, 25, 12, 48, 90, 76, 42],
          },
          {
            type: "bar",
            label: "Dataset 2",
            backgroundColor: "#66BB6A",
            data: [21, 84, 24, 75, 37, 65, 34],
          },
          {
            type: "bar",
            label: "Dataset 3",
            backgroundColor: "#FFA726",
            data: [41, 52, 24, 74, 23, 21, 32],
          },
        ],
      },
      basicOptions: {
        plugins: {
          legend: {
            labels: {
              color: "#495057",
            },
          },
        },
        scales: {
          x: {
            ticks: {
              color: "#495057",
            },
            grid: {
              color: "#ebedef",
            },
          },
          y: {
            ticks: {
              color: "#495057",
            },
            grid: {
              color: "#ebedef",
            },
          },
        },
      },
      horizontalOptions: {
        indexAxis: "y",
        plugins: {
          legend: {
            labels: {
              color: "#495057",
            },
          },
        },
        scales: {
          x: {
            ticks: {
              color: "#495057",
            },
            grid: {
              color: "#ebedef",
            },
          },
          y: {
            ticks: {
              color: "#495057",
            },
            grid: {
              color: "#ebedef",
            },
          },
        },
      },
      multiAxisOptions: {
        plugins: {
          legend: {
            labels: {
              color: "#495057",
            },
          },
          tooltips: {
            mode: "index",
            intersect: true,
          },
        },
        scales: {
          x: {
            ticks: {
              color: "#495057",
            },
            grid: {
              color: "#ebedef",
            },
          },
          y: {
            type: "linear",
            display: true,
            position: "left",
            ticks: {
              min: 0,
              max: 100,
              color: "#495057",
            },
            grid: {
              color: "#ebedef",
            },
          },
          y1: {
            type: "linear",
            display: true,
            position: "right",
            grid: {
              drawOnChartArea: false,
              color: "#ebedef",
            },
            ticks: {
              min: 0,
              max: 100,
              color: "#495057",
            },
          },
        },
      },
      stackedOptions: {
        plugins: {
          tooltips: {
            mode: "index",
            intersect: false,
          },
          legend: {
            labels: {
              color: "#495057",
            },
          },
        },
        scales: {
          x: {
            stacked: true,
            ticks: {
              color: "#495057",
            },
            grid: {
              color: "#ebedef",
            },
          },
          y: {
            stacked: true,
            ticks: {
              color: "#495057",
            },
            grid: {
              color: "#ebedef",
            },
          },
        },
      },
    },
    PieChartData: {
      chartData: {
        labels: ["A", "B", "C"],
        datasets: [
          {
            data: [300, 50, 100],
            backgroundColor: ["#42A5F5", "#66BB6A", "#FFA726"],
            hoverBackgroundColor: ["#64B5F6", "#81C784", "#FFB74D"],
          },
        ],
      },
      lightOptions: {
        plugins: {
          legend: {
            labels: {
              color: "#495057",
            },
          },
        },
      },
    },
    RadarChartData: {
      chartData: {
        labels: [
          "Eating",
          "Drinking",
          "Sleeping",
          "Designing",
          "Coding",
          "Cycling",
          "Running",
        ],
        datasets: [
          {
            label: "My First dataset",
            backgroundColor: "rgba(179,181,198,0.2)",
            borderColor: "rgba(179,181,198,1)",
            pointBackgroundColor: "rgba(179,181,198,1)",
            pointBorderColor: "#fff",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(179,181,198,1)",
            data: [65, 59, 90, 81, 56, 55, 40],
          },
          {
            label: "My Second dataset",
            backgroundColor: "rgba(255,99,132,0.2)",
            borderColor: "rgba(255,99,132,1)",
            pointBackgroundColor: "rgba(255,99,132,1)",
            pointBorderColor: "#fff",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(255,99,132,1)",
            data: [28, 48, 40, 19, 96, 27, 100],
          },
        ],
      },
      chartOption: {
        plugins: {
          legend: {
            labels: {
              color: "#495057",
            },
          },
        },
        scales: {
          r: {
            pointLabels: {
              color: "#495057",
            },
            grid: {
              color: "#ebedef",
            },
            angleLines: {
              color: "#ebedef",
            },
          },
        },
      },
    },
    DoughnutChartData: {
      chartData: {
        labels: ["A", "B", "C"],
        datasets: [
          {
            data: [300, 50, 100],
            backgroundColor: ["#FF6384", "#36A2EB", "#FFCE56"],
            hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56"],
          },
        ],
      },
      lightOptions: {
        plugins: {
          legend: {
            labels: {
              color: "#495057",
            },
          },
        },
      },
    },
    TableData: {
      products: [
        {
          id: "1000",
          code: "f230fh0g3",
          name: "Bamboo Watch",
          description: "Product Description",
          image: "bamboo-watch.jpg",
          price: 65,
          category: "Accessories",
          quantity: 24,
          inventoryStatus: "INSTOCK",
          rating: 5,
        },
        {
          id: "1001",
          code: "nvklal433",
          name: "Black Watch",
          description: "Product Description",
          image: "black-watch.jpg",
          price: 72,
          category: "Accessories",
          quantity: 61,
          inventoryStatus: "INSTOCK",
          rating: 4,
        },
        {
          id: "1002",
          code: "zz21cz3c1",
          name: "Blue Band",
          description: "Product Description",
          image: "blue-band.jpg",
          price: 79,
          category: "Fitness",
          quantity: 2,
          inventoryStatus: "LOWSTOCK",
          rating: 3,
        },
        {
          id: "1003",
          code: "244wgerg2",
          name: "Blue T-Shirt",
          description: "Product Description",
          image: "blue-t-shirt.jpg",
          price: 29,
          category: "Clothing",
          quantity: 25,
          inventoryStatus: "INSTOCK",
          rating: 5,
        },
        {
          id: "1004",
          code: "h456wer53",
          name: "Bracelet",
          description: "Product Description",
          image: "bracelet.jpg",
          price: 15,
          category: "Accessories",
          quantity: 73,
          inventoryStatus: "INSTOCK",
          rating: 4,
        },
      ],
    },
    MultiSelectChipData : {
      selectedData: null,
      options: [
        {name: 'Accounts Clerk Level 1', code: 'AC'},
        {name: 'Accounts Executive', code: 'AE'},
        {name: 'Developer', code: 'DEV'},
        {name: 'Designer', code: 'DES'},
        {name: 'Content Creator', code: 'CC'}
    ],
    }
  },
  //to change the data in the state - commit a mutation - cannot do api execution - no async code (waiting)
  //commit a muatation
  mutations: {
    addItems(state) {
      var obj = { label: "iPhone" };
      state.items.push(obj);
    },
    addItemsByAction(state, payload) {
      var obj = { label: payload };
      state.items.push(obj);
    },
  },
  //access data in a state but not modify - allows async code - api call the commit mutatuon within action
  //dispatch actions
  actions: {
    addItemsDispatch({ commit }) {
      commit("addItemsByAction", "ipad");
    },
  },
  //change/filter data before data is made available to all components of the app
  getters: {
    modifyAddItems(state) {
      // console.log(state);
      state.items[0].label = "Mac";
    },
  },
  //breaks stores into many parts
  modules: {},
});
